package com.vijay.SpringBootProjectWithSecurity.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vijay.SpringBootProjectWithSecurity.dto.UserDto;
import com.vijay.SpringBootProjectWithSecurity.serviceImpl.UserJdbcServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1")
@Api(value="User API", description="User API description", tags="User tag")
public class UserController {
	
	@Autowired
	UserJdbcServiceImpl userImpl;
	
	//If no method is provided it defaults to all HTTP verbs
	@RequestMapping(path="/users", produces= {"application/xml", "application/json"})

	@ApiOperation(value= "Post method  to get Users", notes="Not password protected", response=UserDto.class)
	@ApiResponses(value= {
			 @ApiResponse(code = 401, message = "You are not authorized access the resource"),
	            @ApiResponse(code = 404, message = "The resource not found"),
			@ApiResponse(code=200, message = "Successfully retrieved user")
	})
	public ResponseEntity<UserDto> getUsers(){
		List<UserDto> userList = new ArrayList<UserDto>();
		UserDto user1 = new UserDto();
		user1.setFirstName("vijay");
		user1.setLastName("kumar");
		
		UserDto user2 = new UserDto();
		user2.setFirstName("shanmugam");
		user2.setLastName("shanmugam");
		
		userList.add(user1);
		userList.add(user2);
		
		//sampleway of deserliazing.. example
		String users = "[{'firstname':'Richard', 'lastname':'Feynman'},{'firstname':'Marie','lastname':'Curie'}]";
		
//		Gson gson = new Gson();
//		//this will give us a type to handle list..DESERIALIE lists
		
		//IMPORTANT - below can be used during DESER
//		Type userStringList =new  TypeToken<ArrayList<UserDto>>(){}.getType();
//		List<UserDto> deserializedUserList = gson.fromJson(users, userStringList);
//		
//		deserializedUserList.forEach(i -> System.out.println(i.getFirstName()));
		
		//Now just return a UserDTO
		
		return new ResponseEntity<UserDto>(user1, HttpStatus.OK);
	}
	
	@RequestMapping(path="/users/{id}", produces= {"application/xml", "application/json"}, method=RequestMethod.GET)

	@ApiOperation(value= "Get method  to get Users by Id", notes="Not password protected", response=UserDto.class)
	@ApiResponses(value= {
			 @ApiResponse(code = 401, message = "You are not authorized access the resource"),
	            @ApiResponse(code = 404, message = "The resource not found"),
			@ApiResponse(code=200, message = "Successfully retrieved user")
	})
	public ResponseEntity<UserDto> getUserById(@PathVariable("id") Integer id  ){
		UserDto userDto = userImpl.getUserById(id);
		return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
	}
}
