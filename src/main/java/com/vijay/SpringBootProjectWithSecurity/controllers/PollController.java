package com.vijay.SpringBootProjectWithSecurity.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vijay.SpringBootProjectWithSecurity.dto.Poll;
import com.vijay.SpringBootProjectWithSecurity.dto.PollList;
import com.vijay.SpringBootProjectWithSecurity.exceptions.ErrorObject;
import com.vijay.SpringBootProjectWithSecurity.exceptions.InvalidPollException;
import com.vijay.SpringBootProjectWithSecurity.producer.ProducerKafka;
import com.vijay.SpringBootProjectWithSecurity.serviceImpl.PollJdbcSericeImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;

@RestController
@RequestMapping("/v1")
//@SwaggerDefinition(info = @Info(
//        description = "My API",
//        version = "V1.2.3",
//        title = "The only API you'll ever need to learn about me",
//        termsOfService = "share and care",
//        contact = @Contact(name = "Sponge-Bob", email = "sponge-bob@swagger.io", url = "http://swagger.io"),
//        license = @License(name = "Apache 2.0", url = "http://www.apache.org")),
//
//consumes = {"application/json" },
//produces = {"application/json" },
//schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS},
//externalDocs = @ExternalDocs(value = "About me", url = "http://about.me/me")
//)
@Api(value = "user",  description="Poll API", tags = "User API")
public class PollController {

	@Autowired
	PollJdbcSericeImpl pollJdbcImpl;
	
	@Autowired
	ProducerKafka producerKafka;
	
	@RequestMapping(produces=  {"application/xml", "application/json"}, path="/getPolls/{id}",
			method=RequestMethod.POST, consumes={"application/xml", "application/json"})
	//there is an authorization property for api operations

	@ApiOperation(value= "Post method  to get all Polls", notes="Not password protected", response=PollList.class)
	@ApiResponses(value= {
			@ApiResponse(code=400, message = "Error in calling getPolls"),
			@ApiResponse(code=200, message = "Successfully retrieved messages")
	})
	//@ExceptionHandler(value= {InvalidPollException.class, HttpMediaTypeNotSupportedException.class, HttpMediaTypeNotAcceptableException.class})
	public ResponseEntity<PollList> getAllPolls(@RequestBody Poll poll, @PathVariable("id") Integer a){
			//throws InvalidPollException, HttpMediaTypeNotAcceptableException,
	//HttpMediaTypeNotSupportedException{
		System.out.println("Integer of path var:"+a);
		//List<Poll> response = pollJdbcImpl.getPolls(poll.getId());
		
		//Throwing a custom exception
//		if(poll.getId() ==1 ) {
//			throw new InvalidPollException("Poll id is invalid. Please enter a proper id");
//		}
		
		
		PollList response  = new PollList();
		List<Poll> pollList = new ArrayList<Poll>();
		//this is the event that publishes the message to kafka producer. 
		//We run a consumer that automatically consumers it
		//producerKafka.sendMessage(g.toJson(poll));
		
		pollList.add(poll);  
		response.setPollList(pollList);
		
		return new ResponseEntity<PollList>(response,HttpStatus.OK);
	}
}
