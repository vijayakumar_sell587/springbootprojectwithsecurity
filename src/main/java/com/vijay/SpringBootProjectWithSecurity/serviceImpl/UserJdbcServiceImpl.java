package com.vijay.SpringBootProjectWithSecurity.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vijay.SpringBootProjectWithSecurity.dto.UserDto;

public class UserJdbcServiceImpl {
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	EntityManagerFactory entityManagerFactory;
	
	//IF IN CASE we have not implemented an entityManager then spring boot automatically
	//defines a entitymanager bean
	//EnitytManager is an interface and is so short lived
	//so it creates a bean of SharedEntityManagerInvocationHandler at runtime
	
	//IF WE HAVE DEFINED OUR OWN then use the entitymanager to get the session. because it is where in session
	// the persistence context resides and how the objects can be persisted to the database
	
	//@PersistenceContext
	//EntityManager entityManager;
	
//	@Transactional(propagation=Propagation.REQUIRED)
//	public List<UserDto> getAllUsers(){
//		Session session = entityManagerFactory.unwrap(SessionFactory.class).getCurrentSession();
//		
//		//check if id is not less than 0
//		// no need to use criteria. We can directly use queries to achieve results
//		Query query = session.createNativeQuery("Select * from User");
//		List<UserDto> userList =  query.getResultList();
//		if(!userList.isEmpty()) {
//			return userList;
//		}else {
//			return null;
//		}
//	}
	
//	@Transactional(propagation=Propagation.REQUIRED)
//	
//	public UserDto getUserById(Integer id) {
//		EntityManager entityManager = entityManagerFactory.createEntityManager();
//		Session session = (Session)entityManager.getDelegate();
//		//check if id is not less than 0
//		// no need to use criteria. We can directly use queries to achieve results
//		org.hibernate.query.Query query = session.createNativeQuery("Select First_Name, Last_Name, id from users where id = :aid").setParameter("aid", id).addEntity(UserDto.class);
//				//.setParameter("userId", id);
//		@SuppressWarnings("unchecked")
//		List<UserDto> userList = (List<UserDto>) query.getResultList();
//		System.out.println(userList);
//		
//		return userList.get(0);
//		
//		
//	}
	
	public UserDto getUserById(Integer id) {
		UserDto u = new UserDto();
		u.setFirstName("sdf");
		u.setLastName("sds");
		u.setId(1);
		
		return u;
	}
	
	public void saveUser(UserDto userDto) {
		if(userDto != null) {
			Session session = entityManagerFactory.unwrap(SessionFactory.class).getCurrentSession();
			session.save(userDto);
		}
	}
	
}
