package com.vijay.SpringBootProjectWithSecurity.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import com.vijay.SpringBootProjectWithSecurity.consumer.ConsumerKafka;

@Configuration
@Profile("QA")
public class SpringKafkaConsumer {
	
	
//	@Autowired
//	  Environment env;
//	
//	 /**
//	  * Consumer Config Starts
//	  */
//	 @Bean
//	 KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
//	      ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
//	      factory.setConsumerFactory(consumerFactory());
//	       return factory;
//	    }
//	
//	 @Bean
//	    public ConsumerFactory<String, String> consumerFactory() {
//
//	       // DefaultKafkaConsumerFactory factory=new DefaultKafkaConsumerFactory<>(consumerConfigs());
//	        return new DefaultKafkaConsumerFactory<String, String>(consumerConfigs());
//	    }
//	 
//	 
//	 @Bean
//	    public ConsumerKafka listener() {
//	        return new ConsumerKafka();
//	    }
//	
//	
//	
//	
//	////////////////////////////
//	 @Bean
//	public Map<String, Object> consumerConfigs() {
//		Map<String, Object> consumerConfigs = new HashMap<>();
//		consumerConfigs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, env.getProperty("kafka.broker"));
//		consumerConfigs.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, env.getProperty("enable.auto.commit"));
//		consumerConfigs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, env.getProperty("kafka.auto.commit.reset"));
//		consumerConfigs.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, env.getProperty("auto.commit.interval.ms"));
//		consumerConfigs.put(ConsumerConfig.GROUP_ID_CONFIG, env.getProperty("group.id"));
//		consumerConfigs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class);
//		consumerConfigs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class);
//		
//		return consumerConfigs;
//	}
}
