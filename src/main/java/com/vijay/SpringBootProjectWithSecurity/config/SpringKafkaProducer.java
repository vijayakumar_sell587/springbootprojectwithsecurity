package com.vijay.SpringBootProjectWithSecurity.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
@Profile("QA")
public class SpringKafkaProducer {
	
//	@Autowired
//	Environment env;
//	
//	//Populate the configs map which inturn used for the message channel connection
//	public Map<String, Object> producerConfigs() {
//		Map<String, Object> configs = new HashMap<String, Object>();
//		configs.put(ProducerConfig.ACKS_CONFIG, "all");
//		configs.put(ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG, new Integer(5000));
//		configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, env.getProperty("kafka.broker"));
//		configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class);
//		configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class);
//		return configs;
//	}
//	
//	@Bean
//	public ProducerFactory<String, Object> producerFactory() {
//		return new DefaultKafkaProducerFactory<String, Object>(producerConfigs());
//	}
//	
//	
//	//Kafka template bean allows to send messages to the topic using send method
//	@Bean
//	public KafkaTemplate<String, Object> kafkaTemplate() {
//		return new KafkaTemplate<String, Object>(producerFactory());
//	}
}
