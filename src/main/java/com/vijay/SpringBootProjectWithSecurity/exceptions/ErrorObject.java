package com.vijay.SpringBootProjectWithSecurity.exceptions;

public class ErrorObject {
	
	private String message;
	private org.springframework.http.HttpStatus HttpStatus;
	private int ErrorCode;
	
	public ErrorObject(int errorCode, org.springframework.http.HttpStatus status, String message) {
		this.message = message;
		this.ErrorCode = errorCode;
		this.HttpStatus = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public org.springframework.http.HttpStatus getHttpStatus() {
		return HttpStatus;
	}

	public void setHttpStatus(org.springframework.http.HttpStatus httpStatus) {
		HttpStatus = httpStatus;
	}

	public int getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(int errorCode) {
		ErrorCode = errorCode;
	}
	
	
}
