package com.vijay.SpringBootProjectWithSecurity.configuration;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/application_default.properties")
@Profile("Prod")
public class DataSourceBeanDefault 
//implements DataSourceInterface
{
	
	@Autowired
	private Environment env;
	
//	@Bean
//	public DataSource getDataSource() {
//		BasicDataSource dataS = new BasicDataSource();
//		dataS.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
//		dataS.setUrl(env.getProperty("spring.datasource.url"));
//		dataS.setUsername(env.getProperty("spring.datasource.username"));
//		dataS.setPassword(env.getProperty("spring.datasource.password"));
//		dataS.setMaxTotal(Integer.parseInt(env.getProperty("spring.datasource.dbcp2.max-total")));
//		return dataS;
//	}
}
