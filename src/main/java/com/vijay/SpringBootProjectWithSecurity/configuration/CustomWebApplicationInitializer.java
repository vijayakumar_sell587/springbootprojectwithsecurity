package com.vijay.SpringBootProjectWithSecurity.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class CustomWebApplicationInitializer  implements WebApplicationInitializer{

	@Override
	 public void onStartup(ServletContext servletContext) throws ServletException {
	         WebApplicationContext context = getContext();
	         servletContext.addListener(new ContextLoaderListener(context));
	         System.out.println("Inside servlet context");
	         ServletRegistration.Dynamic dispatcher = servletContext.addServlet("Dispatcher", new DispatcherServlet(context));
	         servletContext.addListener(new ContextLoaderListener());
	         dispatcher.setLoadOnStartup(1);
	         dispatcher.addMapping("*.");
	     }
	 
	 private AnnotationConfigWebApplicationContext getContext() {
		         AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		         context.setConfigLocations("com.vijay.SpringBootProjectWithSecurity.configuration", "com.vijay.SpringBootProjectWithSecurity.config");
		         return context;
		     }


}
