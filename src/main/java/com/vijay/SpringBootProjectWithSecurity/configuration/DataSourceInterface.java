package com.vijay.SpringBootProjectWithSecurity.configuration;

import javax.sql.DataSource;

public interface DataSourceInterface {
	
	public DataSource getDataSource();
}
